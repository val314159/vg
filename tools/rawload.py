from prelude import *

from dm.db import PUT

def main():
    data = json.load(sys.stdin)
    for k,v in data:
        PUT( k, v )
        pass
    pass

if __name__=='__main__': main()
