from prelude import *
from dm.db import ZAP,RNG
from dm.props import *
from dm.load import *

if __name__=='__main__':
    print "new main"
    ZAP()

    u0 = u = 'SYS-TEM'
    addVprops(((u,'name','SYSTEM'),
               ))

    u01 = u = mkUuid()
    addVprops(((u,'name','USER'),
               ))

    u02 = u = mkUuid()
    addVprops(((u,'email','val@ccl.io'),
               (u,'admin',True),
               (u,'_password','password'),
               (u,'ig.username','password1'),
               (u,'_ig.password','password2'),
               (u,'fb.username','password3'),
               (u,'_fb.password','password4'),
               (u,'sc.username','password5'),
               (u,'_sc.password','password6'),
               (u,'vn.username','password7'),
               (u,'_vn.password','password8')))

    u03 = u = mkUuid()
    addVprops(((u,'email','dude@circleclick.com'),
               (u,'_password','d00d')))

    u04 = u = mkUuid()
    addVprops(((u,'email','annebot@ccl.io'),
               (u,'admin',True),
               (u,'_password','ab')))

    u1 = u = mkUuid()
    addVprops(((u,'name','val'),
               (u,'likes','cats'),
               (u,'writes','C++')))

    u2 = u = mkUuid()
    addVprops(((u,'name','annebot'),
               ))

    u3 = u = mkUuid()
    addVprops(((u,'name','buck'),
               (u,'color','white')))

    u4 = u = mkUuid()
    addVprops(((u,'name','lex'),
               (u,'color','black')))

    addEprop(u0,'root',u1)
    addEprop(u0,'type',u01)
    addEprop(u01,'has',u02)
    addEprop(u01,'has',u03)
    addEprop(u01,'has',u04)

    addEprop(u1,'married',u2)
    addEprop(u1,'owns',u3)
    addEprop(u1,'owns',u4)
    addEprop(u4,'bro',u3)
    addEprop(u3,'bro',u4)

    print '=-'*40
    PP(RNG(),'DUMP5----------')
    print '=-'*40

    vValue = list(getVprops(u1))
    PP(vValue,"VV")

    eValue = list(getEprops(u1))
    PP(eValue,"EV")

    veValue = list(getVEprops(u1))
    PP(veValue,"VE")
    
    print '.  '*40

    d = loadVE(u1)
    PP(d)

    newValue = dict(ts=now(), data='Python')
    print "VAL", newValue
    oldValue = getVprop(u1,'writes')
    print "OLD", oldValue
    updVprop(u1,'writes',newValue,oldValue)

    print '=-'*40
    PP(RNG(),'DUMP5----------')
    print '=-'*40

    print "QQQQQQQ"

    PP(getAllVtxUuids())

    for x in getAllVtxUuids():
        PP(loadVE(x),'-      -       -')
