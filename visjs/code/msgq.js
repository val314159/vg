///////////////////////////////////////////
function send_msg(msg) {
    sendGet(q_prefix+"send_msg/*/"+str(msg),function(ret1){
	    //out("THX:"+str(ret1));
	});
}
function send_msgs(msgs) { _(msgs).each(send_msg); }
///////////////////////////////////////////
var Loop = null;
var Since = 0;
function cancelloop() { if (Loop) clearTimeout(Loop); Loop=null; }
function loopme(doAgain) {
    if (undef(doAgain))
	doAgain = true;
    sendGet(q_prefix+"since_msg/*/"+str(Since),
	    function(ret1){
		_(ret1.response).each(function(item){
			var fnTuple = item[1];
			out("fnTuple:", str(fnTuple));
			var fnName = item[1][1];
			var fn = Stubs[fnName];
			var args = item[1][2];
			var kw = item[1][3];
			var meta = item[1][4];
			var rr = fn.apply(kw,args);
			if (!undef(rr))
			    out("RR",str(rr));

			return Since = parseInt( item[0].split(':')[1] )+1;
		    });
		if (doAgain)
		    Loop = setTimeout(loopme,350);
	    });
}
///////////////////////////////////////////
