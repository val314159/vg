var nodes = [];
var edges = [];
var options = { hover: true,
		navigation: true,
		stabilize: false,
		edges: 
		{ width: 3,
		  style: 'arrow'}};
var network = null;
var EDGE_LENGTH = 50;

function E(x){return document.getElementById(x)}
function str(x){return JSON.stringify(x)}
function n(id){ return (str(["N",id])); }
function e(src,name,dst){ return (str(["E",src,name,dst])); }
function mkNetwork(id){
    var vtxs = new vis.DataSet(nodes);
    var edgs = new vis.DataSet(edges);
    var x = {nodes:vtxs,edges:edgs,options:options};
    network=new vis.Network(E(id),x);
    return x;
}
function pushN(id,connectionCount) {
    nodes.push({id: id,
		label: '' + id,
		title: function(){return n(''+id)}});
    if (connectionCount)  connectionCount[id] = 0;
}
function pushE(from,label,to,connectionCount){
    edges.push({from: from,
		to: to,
		length: EDGE_LENGTH,
		label: label,
		title: function(){return e(from,label,to)}});
    if(connectionCount){
	connectionCount[from]++;
	connectionCount[to]++;
    }
}

function load() {
    var connectionCount = [];
    for (var i=91; i<96; i++)  pushN(i,connectionCount);
    pushE(91,"qq",92,connectionCount);
    pushE(91,"ww",92,connectionCount);
    pushE(91,""  ,93,connectionCount);
    pushE(92,".1",94,connectionCount);
    pushE(92,".2",94,connectionCount);
    pushE(92,""  ,95,connectionCount);
    // randomly create some nodes
    var nodeCount = 20;
    var cols = parseInt(Math.sqrt(nodeCount));
    for (var i = 0; i < nodeCount; i++) {
	pushN(i,connectionCount);

        // create links in a scale-free-network way
        if (i == 1) {
	    pushE(i,"controls",0,connectionCount);
        } else if (i > 1) {
	    var conn = edges.length * 2;
	    var rand = Math.floor(Math.random() * conn);
	    var cum = 0;
	    for (var j=0; (j < connectionCount.length && cum < rand); j++) {
		cum += connectionCount[j];
	    }
	    pushE(i,"owns",j,connectionCount);
        }
    }
}

var handle = null;

function draw() {
    load();
    handle = mkNetwork('mynetwork');    
}

var lastId=999;
var nextId=function(){return'id'+(++lastId)};

function xx() {
    console.log("1111");

    var id1 = nextId();
    var id2 = nextId();

    console.log(str([id1,id2]));

    handle.nodes.add([{
		id:id1,
		    title: function(){return n(''+id1)}
	    },
	    {
		id:id2,
		    title: function(){return n(''+id2)}
	    }]);
    handle.edges.add([{
		from:id1,name:'qwert',to:id2,
		    title: function(){return e(id1,'qwert',id2)}
	    }]);

    //    alert("X");
}
