function Stub() {}
Stub.prototype.setProp = function(k,v) { graf.setProp(k,v); };
Stub.prototype.getProps= function(arr) { graf.getProps(arr); };
var Stubs = new Stub();
///////////////////////////////////////////
function draw() {
    handle={nodes: new vis.DataSet([]),
	    edges: new vis.DataSet([]),
	    options:options};
    network=new vis.Network(E('mynetwork'),handle);
    zap_msgs(test1_5);
}
var W = Graph.wrap;
var u = 'setProp';
function test1_5() {
    send_msgs([[0,u,[['000-000','v','a','','',''],W("2qqq")],{}],
	       [0,u,[['200-000','v','b','','',''],W("2qqq")],{}],
	       [0,u,[['100-200','v','c','','',''],W("3qqq")],{}],
	       [0,u,[['100-300','v','d','','',''],W("4qqq")],{}],
	       [0,u,[['100-300','v','e','','',''],W("5qqq")],{}],
	       [0,u,[['100-300','e','f','','200-000',''],W(true)],{}]
	       ]);
    send_msgs([[0,u,[['100-300','v','e','','',''],W("99qqq")],{}]]);
    send_msgs([[0,u,[['100-300','v','e','','',''],{data:"199qqq",ts:0}],{}]]);
    setTimeout(function(){loopme(true);},350);
}
///////////////////////////////////////////
function graf_msg(tuple) { return graf[tuple[1]].apply(graf,tuple[2]); }
function graf_msgs(arr) { _(arr).each(graf_msg); }
function test5() {
    graf_msgs([[0,u,[['000-000','v','a','','',''],W("2qqq")],{}],
	       [0,u,[['200-000','v','b','','',''],W("2qqq")],{}],
	       [0,u,[['100-200','v','c','','',''],W("3qqq")],{}],
	       [0,u,[['100-300','v','d','','',''],W("4qqq")],{}],
	       [0,u,[['100-300','v','e','','',''],W("5qqq")],{}],
	       [0,u,[['100-300','e','f','','200-000',''],W(true)],{}]
	       ]);
    graf_msgs([[0,u,[['100-300','v','e','','',''],W("99qqq")],{}]]);
    graf_msgs([[0,u,[['100-300','v','e','','',''],{data:"199qqq",ts:0}],{}]]);
}
function test1() {
    test5();
    var x = graf.getProps(['100-300']);
    out('2222222');
    out(x);
    out('@@@@@@@@@@@@@');
}
function b5() { loopme(false); }
