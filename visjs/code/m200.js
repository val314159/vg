var nodes = [];
var edges = [];
var options = { hover: true,
		navigation: true,
		stabilize: false,
		edges: 
		{ width: 3,
		  style: 'arrow'}};
var handle = null;
var network = null;
var EDGE_LENGTH = 50;

function E(x){return document.getElementById(x)}
function str(x){return JSON.stringify(x)}
function nnn(id){
    console.log('nnn('+str(id)+')');
    return str(VTX[id]);
}
function eee(src,name,dst){
    console.log('eee('+str([src,name,dst])+')');
    return name;
    return str(EDG[src][name]);
    return (str(["E",src,name,dst]));
}
function mkNetwork(id){
    var vtxs = new vis.DataSet(nodes);
    var edgs = new vis.DataSet(edges);
    var x = {nodes:vtxs,edges:edgs,options:options};
    network=new vis.Network(E(id),x);
    return x;
}
function pushN(id) {
    nodes.push({id: id,
		label: '' + id,
		title: function(){return n(''+id)}});
}
function pushE(from,label,to){
    edges.push({from: from,
		to: to,
		length: EDGE_LENGTH,
		label: label,
		title: function(){return e(from,label,to)}});
}
function sendSnapshotReq() {
    sendGet('http://o.ccl.io:1212/send_snapshot',function(e){
	console.log("send snapshot req returned:");
	console.log("send snapshot req returned:" + str(e));
    });
}
function draw() {
    handle = mkNetwork('mynetwork');
    console.log('so when to send snapshow req?');
}

var lastId=99;
var nextId=function(){return'ID0-'+(++lastId)};

var VTX={};
var EDG={};

function main() {
    var es = new EventSource("http://o.ccl.io:1234/stream");
    es.onmessage = function (e) { sendSnapshotReq(); };
    es.addEventListener('xmessage', function(e){
	    var j = JSON.parse(e.data);
	    var jj = JSON.parse(j);

	    var id = jj['__uuid'];
	    var vv = jj['v'];
	    VTX[id] = jj['v'];
	    var ee = jj['e'];
	    EDG[id] = jj['e'];

	    var label = id;

	    var label0= vv['name'];
	    if (label0) {
		var label1= label0[0];
		if (label1) {
		    label = label1['data'];
		}
	    }
	    label0= vv['email'];
	    if (label0) {
		var label1= label0[0];
		if (label1) {
		    label = label1['data'];
		}
	    }

	    handle.nodes.add([{id:id, label:label,
			    title: function(){return nnn(''+id)}}]);

	    if (ee) {
		for (var kk in ee) {
		    for (var nn=0; nn<ee[kk].length; nn++) {
			var item = ee[kk][nn];
			handle.edges.add([{from:id,label:kk,to:item.__cuid,
					   title:function() {
					       return eee(id,kk,item.__cuid)}}]);
		    }
		}
	    }
	}, false);
}

$(document).ready(main);
