///////////////////////////////////////////
///////////////////////////////////////////
function n(id){
    if (D["N:"+id]) {
	return (D["N:"+id]);
    }
    return (str(["N",id]));
}
function e(src,name,dst){
    if (D["E:"+src+':'+name+':'+dst]) {
	return (D["E:"+src+':'+name+':'+dst]);
    }
    return (str(["E",src,name,dst]));
}
function mkNetwork(id){
    var vtxs = new vis.DataSet(nodes);
    var edgs = new vis.DataSet(edges);
    var x = {nodes:vtxs,edges:edgs,options:options};
    network=new vis.Network(E(id),x);
    return x;
}
function pushN(id,label,arr) {
    nodes.push({id: id,
		label: label || id,
		title: function(){return n(''+id)}});
    D["N:"+id] = '<b>'+id+'</b>'+br+arr.join(br);
}
function pushE(from,label,to,arr){
    edges.push({from: from,
		to: to,
		length: EDGE_LENGTH,
		label: label,
		title: function(){return e(from,label,to)}});
    var key = "E:"+from+':'+label+':'+to;
    D[key] = key;
    if (arr) D[key] = '<b>'+key+'</b>'+br+arr.join(br);
}
function load() {
    sendGet(prefix+"all_vtx_uuids",function(ret1){
	    var results1 = ret1.response.results;
	    var vtxs = results1.join('/');
	    sendGet(prefix+"load_all/"+vtxs,function(ret2){
		    var results2 = ret2.response.results;
		    for (var n=0; n<results2.length; n++) {
			var item = results2[n];
			var uuid = item.__uuid;
			var arr = [];
			for (var q in item.v) {
			    var val = item.v[q][0].data;
			    arr.push(q+'='+((q[0]=='_')?'****':val));
			}
			var label = uuid;
			try {
			    label = item.v.name[0].data;
			} catch (err) {
			    try {
				label = item.v.email[0].data;
			    } catch (err) {
			    }
			}
			pushN(uuid, label,arr);
		    }
		    for (var n=0; n<results2.length; n++) {
			var item = results2[n];
			var uuid = item.__uuid;
			for (var q in item.e) {
			    var item2 = item.e[q];
			    for (var m=0; m<item2.length; m++) {
				pushE(uuid, q, item2[m].__cuid);
			    }
			}
		    }
		    // we got the data, let's do this thing
		    handle = mkNetwork('mynetwork');
		    b5();
		});
	});
}
///////////////////////////////////////////
var handle = null;
function draw() {
    load();
}
///////////////////////////////////////////
var lastId=999;
var nextId=function(){return'id'+(++lastId)};

function xx() {
    console.log("1111");

    var id1 = nextId();
    var id2 = nextId();

    console.log(str([id1,id2]));

    handle.nodes.add([{
		id:id1,
		    title: function(){return n(''+id1)}
	    },
	    {
		id:id2,
		    title: function(){return n(''+id2)}
	    }]);
    handle.edges.add([{
		from:id1,name:'qwert',to:id2,
		    title: function(){return e(id1,'qwert',id2)}
	    }]);
}
///////////////////////////////////////////
function b1() { out("x11"); xx(); }
function b2() { send_msg([0,'out0',[],{},{}]); }
function b3() {
    var u = generateUUID();
    var d = [[u,'email',    {ts:now(),data:'unknown@nowhere.net'}],
	     [u,'_password',{ts:now(),data:'whoami'}]];
    send_msg([0,'setVprops',[d],{},{}]);
}
function b4() { out("cancel loop"); cancelloop(); }
function b5() { loopme(false); }
function b6() { loopme(); }
function dumpGraf() {
    out("======V");
    _(graf.v).forIn(function(v1,k1){
	    out(k1);
	    out('1=====>');
	    out(v1);
	});
    out("======E");
    _(graf.e).forIn(function(v1,k1){
	    out(k1);
	    out('2=====>');
	    out(v1);
	});
    out("======Z");
}
function b7() { dumpGraf(); }
///////////////////////////////////////////
