G.graphs = {};
function Graph() { this.v=[]; this.e=[]; }
Graph.prototype.dump = function() {
    out("======V");
    _(graf.v).forEach(function(v1){ out(v1); });
    out("======E");
    _(graf.e).forEach(function(v1){ out(v1); });
    out("======Z");
};
function keyV(x){ return x.__keys.join('#'); }
Graph.prototype.getPropRange = function(pfx) {
    var pfx1 = _(pfx).clone();
    var pfx2 = _(pfx).clone();
    pfx2.push('~~~');
    var ndx1 = _(this.v).sortedIndex({__keys:pfx1},keyV);
    var ndx2 = _(this.v).sortedIndex({__keys:pfx2},keyV);
    return [ndx1,ndx2,this.v];
};
Graph.prototype.getProps = function(pfx) {
    var ret = this.getPropRange(pfx);
    return ret[2].slice(ret[0],ret[1]);
};
function addGrafNode(id1) {
    handle.nodes.add([{
		id:id1, title: function(){return n(''+id1)}	    }]);}
function addGrafEdge(id1,name,id2) {
    handle.edges.add([{
		from:id1,label:name,to:id2,
		    title: function(){return e(id1,name,id2)}	    }]);}
Graph.prototype.setProp = function(k,v) {
    /* Hello! */
    try {
	if (k[1]=='v') {
	    addGrafNode(k[0]);
	} else if (k[1]=='e') {
	    addGrafEdge(k[0],k[2],k[4]);
	} else {
	}
    } catch (err) {
    }
    v.__keys = k;
    var ndx = _(this.v).sortedIndex(v,keyV);
    xx = this.v[ndx];
    if (undef(xx)) {
	this.v.splice( ndx, 0, v );
    } else if (!arraysEqual(xx.__keys, v.__keys)) {
	this.v.splice( ndx, 0, v );
    } else if (xx.ts < v.ts) {
	this.v[ndx] = v;
    }
};
Graph.wrap = function(x) { return {ts:now(), data:x} };
Graph.prototype.setProps = function(recs) {
    _(recs).forEach(function(x){this.setProp(x[0],x[1])},this);
};
G.graphs.g = new Graph();
var graf = G.graphs.g;
