var prefix = "http://localhost:1212/rpc/";
var q_prefix = "http://localhost:1313/q/";
var D={};
var nodes = [];
var edges = [];
var options = { hover: true,
		navigation: true,
		stabilize: false,
		edges: 
		{ width: 3,
		  style: 'arrow'}};
var network = null;
var br="<br/>";
var EDGE_LENGTH = 80;
function str(x,y) {
    //console.log("SSSTRxxx"+typeof(x));
    if ((''+typeof(x))=="function") return "F:"+x;
    return JSON.stringify(x,null,y);
}
function reload(){ window.location=""; return true; }
function out(m,n){
    var msg = 'unknown';
    if (n===undefined)
	msg = str(m,4);
    else
	msg = m+':'+str(n,4);
    console.log(msg);
}
///////////////////////////////////////////
var offset = 0;
function now() {
    if (++offset >= 1000) offset=0;
    return (new Date()).getTime()*1000+offset;
}
function generateUUID() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
	    var r = (d + Math.random()*16)%16 | 0;
	    d = Math.floor(d/16);
	    return (c=='x' ? r : (r&0x7|0x8)).toString(16);
	});
    return uuid;
}
///////////////////////////////////////////
function Xinsert(data, value) {
    var index = _.sortedIndex( data, value, function( i ) {
	    return -i;    
	});
    data.splice( index, 0, value );
}
function arraysEqual(a, b) {
    if (a === b) return true;
    if (a == null || b == null) return false;
    if (a.length != b.length) return false;
    for (var i = 0; i < a.length; ++i) {
	if (a[i] !== b[i]) return false;
    }
    return true;
}
