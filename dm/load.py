from prelude import *
from props import *

def _loadProps(u1,itr,kids=True):
    d={}
    for k,v in itr:
        if k[1] not in d:
            d[k[1]] = {}
            pass
        if k[2] not in d[k[1]]:
            d[k[1]][k[2]] = []
            pass
        vv = dict(v)
        if k[5]: vv['__name2'] = k[5]
        if k[4]:
            vv['__cuid' ] = k[4]
            if kids:
                if 'k' not in d: d['k']={}
                d['k'][k[4]] = loadV(k[4])['v']
                pass
            pass
        if k[3]: vv['__ndx'  ] = k[3]
        d[k[1]][k[2]].append( vv )
        pass
    d['__uuid']=u1
    return d

def loadV (u1,*a,**kw): return _loadProps(u1,getVprops (u1,*a,**kw))
def loadVE(u1,*a,**kw): return _loadProps(u1,getVEprops(u1,*a,**kw))
def loadE (u1,*a,**kw): return _loadProps(u1,getEprops (u1,*a,**kw))

