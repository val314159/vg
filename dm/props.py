from prelude import *
from db import *

def wrap(v): return dict(ts=now(),data=v)

def setVprop(u,k,v):     PUT([u, 'v',k,'','',''], v); return u
def setEprop(u1,k,u2,v): PUT([u1,'e',k,'',u2,''], v)

def addVprop(u,k,v): return setVprop(u or mkUuid(),k,wrap(v))
def addEprop(u1,k,u2,v=True):return setEprop(u1,k,u2,wrap(v))

def addVprops(arr): return [addVprop(*a) for a in arr]
def addEprops(arr): return [addEprop(*a) for a in arr]
def setVprops(arr): return [setVprop(*a) for a in arr]
def setEprops(arr): return [setEprop(*a) for a in arr]

def getVEprops(u): return RNG([u,''])

def getVprops(u,k=''): return RNG([u,'v',k])
def getVprop (u,k):    return GET([u,'v',k,'','',''])

def getEprop (u,k,u2): return GET([u,'e',k,'',u2,''])
def getEprops(u,k='',u2=None):
    if u2: r=[u,'e',k,'',u2,'']
    else : r=[u,'e',k]
    return RNG(r)

def updVprop(u1,name,newValue,oldValue,deleted=False):
    if deleted: newValue['deleted'] = True
    if oldValue['ts'] < newValue['ts']:
        setVprop(u1,name,newValue)
        
def updEprop(u1,name,u2,newValue,oldValue,deleted=False):
    if deleted: newValue['deleted'] = True
    if oldValue['ts'] < newValue['ts']:
        setEprop(u1,name,u2,newValue)

def getAllVtxUuids():
    lx = ''
    for k,v in RNG([]):
        x = k[0]
        if lx!=x:
            lx=x
            yield lx

