from prelude import *
from gevent import monkey; monkey.patch_all()

from bottle import route, run, request, response
from jrpc import rpc_resp

@route('/stream')
def stream():
    from time import sleep
    yield 'START'
    sleep(3)
    yield 'MIDDLE'
    sleep(5)
    yield 'END'
    pass

import stubs.q as stubs

rpcPrefix='/q/'
@route(rpcPrefix+'<function_name>',            method=['GET','POST'])
@route(rpcPrefix+'<function_name>/<path:path>',method=['GET','POST'])
@route(rpcPrefix+'<function_name>/<path>',     method=['GET','POST'])
def q_rpc(function_name,path=None):
    return rpc_resp([0,function_name,path,{}],stubs,add_headers_to=response)

def rpc_listen(stub):
    def inner(function_name,path=None):
        return rpc_resp([0,function_name,path,{}],stub,add_headers_to=response)
    return inner

#rpcPrefix='/q/'
#@route(rpcPrefix+'<function_name>',            method=['GET','POST'])
#@route(rpcPrefix+'<function_name>/<path:path>',method=['GET','POST'])
#@route(rpcPrefix+'<function_name>/<path>',     method=['GET','POST'])
#xq_rpc = rpc_listen(stubs)


if __name__=='__main__': run(host='', port=getarg(1,1313), server='gevent', debug=True)
