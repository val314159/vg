
from dm.load import *

def all_vtx_uuids():
   return dict(results=list(getAllVtxUuids()))

def load_all(*args):
   return dict(results=list(loadVE(a) for a in args))

def rawload(s):
   data = json.loads(s)
   for k,v in data['response']['results']:
      PUT( k, v )
   return dict(results=list(RNG()))

def rawdump():
   return dict(results=list(RNG()))

##################################
##################################

def create_user(email,password):
   u = mkUuid()
   d = dict(email=email,password=password,uuid=u)
   send_msg("*",d)
   return dict(results=list('xyz', d))
