import leveldb

_MsgDB = leveldb.LevelDB('.msgs')

from prelude import now_micro_str,micro_str

def _chan(channel,ts): return channel+':'+str(ts)

def send_msg(channel,msg):
   return _MsgDB.Put(_chan(channel,now_micro_str()),msg)

def since_msg(channel,since):
   import json
   return [(kk,json.loads(vv)) for kk,vv in 
           _MsgDB.RangeIter(_chan(channel,
                                  micro_str(long(since))))]

def zap_msgs():
   return [(kk,_MsgDB.Delete(kk))[0]
           for kk,vv in _MsgDB.RangeIter()]
