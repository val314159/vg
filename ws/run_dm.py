from prelude import *
from gevent import monkey; monkey.patch_all()

from bottle import route, run, request, response, template

from dm.load import *
from jrpc import rpc_resp

import stubs
import stubs.datamodel as dm

rpcPrefix='/rpc/'
@route(rpcPrefix+'<function_name>',            method=['GET','POST'])
@route(rpcPrefix+'<function_name>/<path:path>',method=['GET','POST'])
@route(rpcPrefix+'<function_name>/<path>',     method=['GET','POST'])
def rpc(function_name,path=None):
    path=path.split('/') if path else []
    return rpc_resp([0,function_name,path,{}],
                    stubs,add_headers_to=response)

rpcPrefix='/dm/'
@route(rpcPrefix+'<function_name>',            method=['GET','POST'])
@route(rpcPrefix+'<function_name>/<path:path>',method=['GET','POST'])
@route(rpcPrefix+'<function_name>/<path>',     method=['GET','POST'])
def dm_rpc(function_name,path=None):
    path=path.split('/') if path else []
    return rpc_resp([0,function_name,path,{}],dm,
                    add_headers_to=response)

def tmpl(x,*args,**kw): return template('tpl/'+x,*args,**kw)

@route('/send_snapshot')
def send_snapshot():
    import cors, requests
    cors.add_headers(response)
    x = requests.get('http://localhost:1212/dm/all_vtx_uuids')
    print "X", x
    j=json.loads(x.content)
    print "J", j
    print j['response']['results']
    print '/'.join(j['response']['results'])
    x = requests.get('http://localhost:1212/dm/load_all/'+'/'.join(j['response']['results']))
    print "X", x
    j=json.loads(x.content)
    print "J", j
    print "J", j['response']['results']
    print '1 == == == >'
    for r in j['response']['results']:
        print ' -> ', r
        y = requests.get('http://localhost:1234/send_all/xmessage/'+json.dumps(r))
        pass
    print '9 == == == >'
    return j

@route('/')
def index(): return tmpl('index.html')

@route('/v_all')
def v_all(): return tmpl('v_all.html', uuids=list(getAllVtxUuids()))

@route('/e/<puid>/<name>//<cuid>')
def e(puid,name,cuid):  return tmpl('e.html', puid=puid, name=name, cuid=cuid)

@route('/v/<uuid>')
def v(uuid): return tmpl('v.html',d=loadVE(uuid))

if __name__=='__main__': run(host='', port=getarg(1,1212), server='gevent', debug=True)
