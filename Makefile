
all:
	@echo hm.

clean:
	find ??* -name \*.pyc | xargs rm
	find ??* -name \*~ | xargs rm

realclean: clean
	rm -fr .db .msgs

superclean: realclean
	rm -fr v
