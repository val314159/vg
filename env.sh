. .v/bin/activate

alias py_ws='python -u -mws.run_dm 1212'
alias q_ws='python -u -mws.run_qx 1313'
alias static_ws='(cd visjs ; python -u -mSimpleHTTPServer 8001)'

alias run_py_ws='(source env.sh ; py_ws)'
alias run_q_ws='(source env.sh ; q_ws)'
alias run_static_ws='(source env.sh ; static_ws)'

export COLUMNS
export LINES
export PYTHONUNBUFFERED=1
