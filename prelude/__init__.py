import os,sys,traceback as tb,json,time,random,types,re,requests
from pprint import pprint
def PP(x='',msg=None):
    if msg is not None: print msg
    if type(x)==types.GeneratorType: x=list(x)
    pprint(x)
def pops(d,*a): return [d.pop(k) for k in a], d
def setdkv(d,k,v): d[k]=v; return d,k,v
def setd  (d,k,v): return setdvk[0]
def setk  (d,k,v): return setdvk[1]
def setv  (d,k,v): return setdvk[2]
def now(): return time.time()
def mkUuid():
    r=lambda:int(random.random()*0x1000)
    return '%03X-%03X' % (r(),r())

def getarg(n,default=None):
    try:    return sys.argv[n]
    except: return default

def now_micro_long(): return long(time.time()*1000000L)
def micro_str(micros):return '%016d' % micros
def now_micro_str (): return micro_str(now_micro_long())

