valgraph
========

v0.0.3
------

__Prerequisites__:

https://github.com/val314159/qsvr

__To install__:

> sh install.sh

__To load environment__:

> source env.sh

__To load initial data set__ _(python server can't be running during this)_:

> python -m tools.rawload <graphs/default.json

or

> python -m tools.populate1

__To run python webserver__ _(port 1212)_:

> run_py_ws

__To run static webserver__ _(port 8000)_:

> run_static_ws

__To dump database (raw)__:

> python -m tools.rawdump | tee g/graph.json

__To zap (delete all)__:

> python -m tools.zap